package com.iotbox;

import java.text.SimpleDateFormat;

public class Util { 
	public static String getNowTime() {
		SimpleDateFormat sDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");     
		String date = sDateFormat.format(new java.util.Date());
		return date;
	}
}
