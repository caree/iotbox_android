package com.iotbox;
 
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import java.lang.Math;  

public class ZigbeeActivity  extends SerialPortActivity {
	EditText mReception;
	EditText mResolve; 
	StringBuilder recBuffer; // 数据接收缓存区 
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState); 
		//打开串口
		openPort(SerialPortType.port_zigbee);
		setContentView(R.layout.console_zigbee);
		TextView titleView = (TextView)findViewById(R.id.widget_navbar_ref).findViewById(R.id.nav_title);
		titleView.setText("传感器读取实验");  
		recBuffer = new StringBuilder(); //初始化
		
		// back
		final Button backBtn = (Button)findViewById(R.id.widget_navbar_ref).findViewById(R.id.btn_back);
		backBtn.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				ZigbeeActivity.this.finish();
			}
		});
		 
		mReception = (EditText)findViewById(R.id.EditTextReception);
		mResolve = (EditText)findViewById(R.id.EditTextResolve);
	}
	
	@Override
	protected void onDataReceived(final byte[] buffer, final int size) {
		runOnUiThread(new Runnable() {
			public void run() {
				if (mReception != null) { 
					String rec = CHexConver.byte2HexStr(buffer, size, false); 
					recBuffer.append(rec); 
					mReception.append(rec); 
					String recBufferString = recBuffer.toString();
					if(recBufferString.startsWith("AA") &&
					   recBufferString.endsWith("0000") && 
					   recBufferString.length() == 58)  {
						mReception.append("\r\n"); 
						decodeData(recBuffer.toString());
						recBuffer.delete(0, 58);
					}
				}
			}
		});
	}  
	
	public void clearRawClick(View v) {
		mReception.setText("");
	}
	
	public void clearResolveClick(View v) {
		mResolve.setText("");
	}
	
//	AA4401FFFFFFFFFFFFFFFF00124B0006110B0805001910010300000000
//  AA4401FFFFFFFFFFFFFFFF00124B0006110B4F440019100103A0850000
//	AA4401FFFFFFFFFFFFFFFF00124B0006110C57410019100103A0860000
	public void decodeData(String rawdata)
    {  
        int type = Integer.parseInt(rawdata.substring(48, 50));
		String data = rawdata.substring(50, 54);
        String zigbeeId = rawdata.substring(22,38);
        mResolve.append(Util.getNowTime() +"          【节点ID】"+ zigbeeId +"       ");
		
        double y = 0;
        switch (type) {
        	//温度 
            case 1:{
		        y = convertTotemp(data);   
		        mResolve.append("【温度】"+String.format("%.1f C", y));
                break;
            }
 
            //湿度
            case 2: {
            	y = convertTohumi(data); 
		        mResolve.append("【湿度】"+String.format("%.1f", y));
            	break;
            }
         
            //光照
            case 3:{
            	y = convertTolit(data);
		        mResolve.append("【光照】"+String.format("%.1f", y));
                break;
            }
        } 
        
        mResolve.append("\n");
    } 
	 
    public static int getCh(String M) {
    	String str = Integer.toString(Integer.parseInt(M, 16), 2);
    	if(str.length()<8) {
    		return 0;
    	}
    	
        int C = Integer.parseInt(str.substring(1, 4),2);
        int S = Integer.parseInt(str.substring(4, 8),2);
        double ADC = (16.5 * (((Math.pow(2, C)) - 1))) + (S * Math.pow(2 , C));
        return (int)Math.floor(ADC);
    }
    
	public double convertTolit(String str) {  
		double lux = 0;
        int ch0 = getCh(str.substring(0, 2));
        int ch1 = getCh(str.substring(2, 4));
        if(ch0 != ch1) {
            lux = (ch0 - ch1) * 0.39 * Math.pow(Math.E, (-0.181 * (Math.pow((ch1 / (ch0 - ch1)), 2))));
        }
        
        return lux;
    }
    
    public double convertTotemp(String str) { 
		String temData;
		double d1 = -39.7, d2 = 0.01;		 
		int SO = Integer.parseInt(str,16);
		String strBin=  Integer.toString(SO,2);		
		if (strBin.length() > 14) {
			temData = strBin.substring(0, 14);
		}
		else { 
			temData = strBin;
		}  
		
		return (d1 + d2 * (Integer.parseInt(temData, 2)));
	}
    
    public double convertTohumi(String str) {
		double c1 = -2.0468, c2 = 0.0367, t1 = 0.01, t2 = 0.00008;
		double c3 = -1.5955E-6;
		int SO = Integer.parseInt("0"+str.substring (1,4), 16);
		return (c1 + c2 * SO + c3 * (SO ^ 2) + (-39.7 - 25) * (t1 + t2 * SO));
	} 
}
	
